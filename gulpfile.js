'use-strict';

const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const less = require('gulp-less');
const watch = require('gulp-watch');
const rimraf = require('rimraf');
const minifyJS = require('gulp-js-minify');
const concat = require('gulp-concat');
const util = require('gulp-util');
const reload = browserSync.reload;

const path = {
    build: { // Here we will put all our ready files
        html: 'build/',
        js: 'build/js/',
        css: 'build/css/',
        img: 'build/img/',
        fonts: 'build/fonts'
    },
    src: { // Folder from were we will take all files to convert
        html: 'src/*.html',
        js: 'src/js/**/*.js',
        css: 'src/css/**/*.less',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/**/*.js',
        css: 'src/**/*.less',
        img: 'src/img/**/*.*',
        fonts: 'src/img/**/*.*'
    },
    clean: './build'
};

const config = {
    server: {
        baseDir: './build'
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: 'Fucking good baby!'
};

gulp.task('html:build', function() {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function() {
    gulp.src(path.src.js)
        // .pipe(sourcemaps.init())
        // .pipe(minifyJS())
        // .pipe(concat('script.js'))
        // .pipe(uglify({ mangle: false }))
        // .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('css:build', function() {
    gulp.src(path.src.css)
        .pipe(sourcemaps.init())
        .pipe(less())
        .pipe(autoprefixer({browsers: ['> 0.01%'], cascade: false}))
        .on('error', console.error.bind(console))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function() {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        .pipe(reload({stream: true}));
});

gulp.task('build', [
    'html:build',
    'js:build',
    'css:build',
    'fonts:build',
    'image:build'
]);

gulp.task('watch', function() {
    watch([path.watch.html], function(event, cd) {
        gulp.start('html:build');
    });
    watch([path.watch.js], function(event, cd) {
        gulp.start('js:build');
    });
    watch([path.watch.css], function(event, cd) {
        gulp.start('css:build');
    });
    watch([path.watch.fonts], function(event, cd) {
        gulp.start('fonts:build');
    });
    watch([path.watch.img], function(event, cd) {
        gulp.start('image:build');
    });
});

gulp.task('webserver', function() {
    browserSync(config);
});

gulp.task('clean', function(cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);