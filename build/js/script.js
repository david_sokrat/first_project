var owl = $('.owl-one');
var owlTwo = $('.owl-two');
var sliderCount = 1;

$(document).ready(function() {
    /* Call owl-carousel plugin */

    owl.owlCarousel({
        loop: true,
        dots: false,
        dotData: false,
        dotsData: false,
        margin: -1,
        responsive: {
            0:{
                items:1
            }
        }
    });

    $('.owl-next').click(function() {
        owl.trigger('next.owl.carousel', [700]);
        sliderCount++;

        if (sliderCount > 4) {
            sliderCount = 1;
        }

        $('.about-project').fadeIn(350);
        $('#about-project-pop-up').remove();
    });
    
    $('.owl-prev').click(function() {
        owl.trigger('prev.owl.carousel', [700]);
        sliderCount--;

        if (sliderCount <= 0) {
            sliderCount = 4;
        }

        $('.about-project').fadeIn(350);
        $('#about-project-pop-up').remove();
    });

    owlTwo.owlCarousel({
        loop: true,
        dots: false,
        dotData: false,
        dotsData: false,
        margin: 0,
        responsive: {
            0:{
                items:1
            },
            500:{
                items:2
            },
            1100:{
                items: 3
            },
            1750: {
                items: 4
            }
        }
    });

    /* Scroll down the page */

    $('#scroll-arrow-btn').click(function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: $('#target-area').offset().top}, 700);
    });

    /* Svg animated icons */

    function isOnScreen(elem) {
        if( elem.length == 0 ) {
            return;
        }

        var $window = jQuery(window);
        var viewport_top = $window.scrollTop();
        var viewport_height = $window.height();
        var viewport_bottom = viewport_top + viewport_height;
        var $elem = jQuery(elem);
        var top = $elem.offset().top;
        var height = $elem.height();
        var bottom = top + height;
    
        return (top >= viewport_top && top < viewport_bottom) ||
        (bottom > viewport_top && bottom <= viewport_bottom) ||
        (height > viewport_height && top <= viewport_top && bottom >= viewport_bottom);
    }
    
    jQuery( document ).ready( function() {
        window.addEventListener('scroll', function(e) {
            if (isOnScreen(jQuery('#gear-icon'))) { 
                animGear.play();
            } else {
                animGear.reset();
            }
            
            if (isOnScreen(jQuery('#chess-icon'))) {
                animChess.play();
            } else {
                animChess.reset();
            }

            if (isOnScreen(jQuery('.bg-logo'))) {
                animBgLogo.play();
            } else {
                animBgLogo.reset();
            }
        });
    });

    /* Generate pop-up about-project */

    $('.about-project').click(function(e) {
        function generatePopUpPromo() {
            e.preventDefault();
            $('.about-project').fadeOut(350);

            const popUpIdsArray = [
                'about-project-pop-up',
                'pop-up-btns',
                'pop-up-project-btn',
                'pop-up-account-btn'
            ];

            const h1DataArray = [
                'Seventh Heaven',
                'Батуми Палас',
                'Аквамарин',
                'Аллея Палас'
            ];

            const aDataArray = [
                'http://7heaven.ge',
                'http://7heaven.ge',
                'http://aquamarine.ge',
                'http://alleypalace.com',
                'На сайт проекта'
            ];

            $('html, body').animate({scrollTop: $('#pop-up-generator').offset().top}, 700);

            for (var i = 0; i < 5; i++) {
                var div = document.createElement('div');
                div.setAttribute('id', popUpIdsArray[i]);
                document.getElementById('pop-up-generator').appendChild(div);
        
                if (i === 2) {
                    var a = document.createElement('a');
                    if (sliderCount === 1) {
                        var aText = document.createTextNode(aDataArray[4]);
                        a.setAttribute('href', aDataArray[0]);
                        var h1Text = document.createTextNode(h1DataArray[0]);
                    }

                    if (sliderCount === 2) {
                        var aText = document.createTextNode(aDataArray[4]);
                        a.setAttribute('href', aDataArray[1]);
                        var h1Text = document.createTextNode(h1DataArray[1]);
                    }

                    if (sliderCount === 3) {
                        var aText = document.createTextNode(aDataArray[4]);
                        a.setAttribute('href', aDataArray[2]);
                        var h1Text = document.createTextNode(h1DataArray[2]);
                    }

                    if (sliderCount === 4) {
                        var aText = document.createTextNode(aDataArray[4]);
                        a.setAttribute('href', aDataArray[3]);
                        var h1Text = document.createTextNode(h1DataArray[3]);
                    }
                    
                    a.setAttribute('target', '_blank');
                    a.appendChild(aText);
                    document.getElementById(popUpIdsArray[2]).appendChild(a);
                }
        
                if (i === 3) {
                    var a = document.createElement('a');
                    var aText = document.createTextNode('Мой кабинет');
                    a.setAttribute('href', '##');
                    a.setAttribute('id', 'profile-pop-up-opener');
                    a.appendChild(aText);
                    document.getElementById(popUpIdsArray[3]).appendChild(a);
                }
            }
            
            var h1 = document.createElement('h1');
            var span = document.createElement('span');
            var icon = document.createElement('i');
            var spanText = document.createTextNode('Резиденция в престижном районе Батуми, качественной инфраструктурой и пляжем в пешей доступности, а также апартаментами с видом на море.');
            h1.appendChild(h1Text);
            h1.appendChild(icon);
            span.appendChild(spanText);
            var popUpDiv = document.getElementById('about-project-pop-up');
            var popUpBtnsDiv = document.getElementById('pop-up-btns');
            popUpDiv.appendChild(h1);
            popUpDiv.appendChild(span);
            popUpDiv.appendChild(popUpBtnsDiv);
            icon.setAttribute('class', 'pop-up-close-icon');
            popUpBtnsDiv.appendChild(document.getElementById('pop-up-project-btn'));
            popUpBtnsDiv.appendChild(document.getElementById('pop-up-account-btn'));
        }

        generatePopUpPromo();

        //Animation for pop-ups in promo block

        $('#profile-pop-up-opener').click(function() {
            $('.pop-up-clients-profile').fadeIn(350);
            $('.dark-background-layer').fadeIn(350);
        });

        $('.pop-up-close-icon').click(function() {
            $('.about-project').fadeIn(350);
            $('#about-project-pop-up').remove();
        });
    });

    //Animation for main info pop-ups

    $('#managment-control').click(function(e) {
        var h1Text = document.createTextNode('Управление недвижимостью');
        document.getElementById('pop-up-managment-h1').appendChild(h1Text);
        $('.pop-up-managment').removeClass('not-visible');
        
        $('html, body').animate({scrollTop: $('#target-area').offset().top}, 700);
        $('.managment-descr-container').fadeOut(350);
        $('.pop-up-managment').fadeIn(350);
        $('.pop-up-marketing').addClass('not-visible');
        $('.pop-up-marketing').addClass('not-visible');
        $('.pop-up-marketing').fadeOut(350);
        $('.marketing-descr-container').fadeIn(350);

        function headerFadeOut() {
            $("#pop-up-marketing-h1").empty();
        }
        
        setTimeout(headerFadeOut, 1000);

        if (200 < $(window).width() && $(window).width() < 900) {
            $('.pop-up-managment').attr('style', 'height: 300px');
        }

        resizer('.pop-up-managment');
    });

    $('#marketing-control').click(function() {
        var h1Text = document.createTextNode('Услуги маркетинга');
        document.getElementById('pop-up-marketing-h1').appendChild(h1Text);
        $('.pop-up-marketing').removeClass('not-visible');
        
        $('html, body').animate({scrollTop: $('#target-area').offset().top}, 700);
        $('.marketing-descr-container').fadeOut(350);
        $('.pop-up-marketing').fadeIn(350);
        $('.pop-up-managment').addClass('not-visible');
        $('.pop-up-managment').fadeOut(350);
        $('.managment-descr-container').fadeIn(350);

        function headerFadeOut() {
            $("#pop-up-managment-h1").empty();
        }
        
        setTimeout(headerFadeOut, 1000);

        if (200 < $(window).width() && $(window).width() < 900) {
            $('.pop-up-marketing').attr('style', 'height: 300px');
        }

        resizer('.pop-up-marketing');
    });

    $('.close-managment-pop-up').click(function() {
        $('.pop-up-managment').fadeOut(350);
        $('.managment-descr-container').fadeIn(350);
        $('.pop-up-managment').addClass('not-visible');

        function headerFadeOut() {
            $("#pop-up-managment-h1").empty();
        }
        
        setTimeout(headerFadeOut, 1000);
    });

    $('.close-marketing-pop-up').click(function() {
        $('.pop-up-marketing').fadeOut(350);
        $('.marketing-descr-container').fadeIn(350);
        $('.pop-up-marketing').addClass('not-visible');

        function headerFadeOut() {
            $("#pop-up-marketing-h1").empty();
        }
        
        setTimeout(headerFadeOut, 1000);
    }); 

    //Animation for profile pop up

    $('#log-in-link').click(function() {  
        $('.pop-up-clients-profile').fadeIn(350);
        $('.dark-background-layer').fadeIn(350);
    });

    $('.close-clients-profile-pop-up').click(function() {
        $('.pop-up-clients-profile').fadeOut(350);
        $('.dark-background-layer').fadeOut(350);
    }); 

    //Animation for burger menu pop up
    /* Change view of burger-menu */

    $('.burger-menu-container').click(function() {
        $('.burger-menu-container').toggleClass('active-burger-pop-up');

        if ($('.burger-menu-container').hasClass('active-burger-pop-up')) {
            $('.burger-menu-pop-up').fadeIn(350);
            $('.burger-menu-container').toggleClass('black-styled-close-button');
            $('.burger-menu-lines').toggleClass('burger-menu-lines-blacked');
            $('.burger-menu-container').attr('style', 'margin-top: -30px !important;');
        } else {
            $('.burger-menu-pop-up').fadeOut(350);
            $('.burger-menu-container').toggleClass('black-styled-close-button');
            $('.burger-menu-lines').toggleClass('burger-menu-lines-blacked');
            $('.burger-menu-container').attr('style', 'margin-top: 10px !important;');
        }
    });
});

function resizer(resizeEl) {
    $(window).resize(function() {
        var margVal = 80;

        if (1400 < $(window).width() && $(window).width() < 2000) {
            margVal = 80;
            $(resizeEl).attr('style', 'width: ' + blockWidth + 'px; height: ' + blockHeight + 'px;');
        }

        if (200 < $(window).width() && $(window).width() < 1399) {
            margVal = 40;
            $(resizeEl).attr('style', 'width: ' + blockWidth + 'px; height: ' + blockHeight + 'px;');
        }

        var blockWidth = $('.item-main').width() - margVal;
        var blockHeight = $('#target-area').height() - margVal;

        if (200 < $(window).width() && $(window).width() < 900) {
            $(resizeEl).attr('style', 'height: 300px');
        }
    });
}

var windowSizeMap = $(window).width();

function initMap() {}

$(() => {
    var uluru = {lat: 41.646341, lng: 41.643392};
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: uluru,
        disableDefaultUI: true,
        styles: [
            {elementType: "geometry", stylers: [{"color": "#f5f5f5"}]},
            {elementType: "labels.icon", stylers: [{visibility: "off" }]}, 
            {elementType: "labels.text.fill", stylers: [{color: "#616161"}]}, 
            {elementType: "labels.text.stroke", stylers: [{color: "#f5f5f5"}]},
            {
                featureType: "administrative.land_parcel", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#bdbdbd" }]
            }, 
            {   
                featureType: "poi", 
                elementType: "geometry", 
                stylers: [{color: "#eeeeee"}]
            }, 
            { 
                featureType: "poi", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#757575"}] 
            }, 
            { 
                featureType: "poi.park", 
                elementType: "geometry", 
                stylers: [{color: "#e5e5e5"}] 
            }, 
            { 
                featureType: "poi.park", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#9e9e9e"}]
            },
            { 
                featureType: "road", 
                elementType: "geometry", 
                stylers: [{color: "#ffffff"}] 
            }, 
            { 
                featureType: "road.arterial", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#757575"}]
            }, 
            { 
                featureType: "road.highway", 
                elementType: "geometry", 
                stylers: [{color: "#dadada"}]
            }, 
            { 
                featureType: "road.highway", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#616161"}] 
            }, 
            { 
                featureType: "road.local", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#9e9e9e"}] 
            }, 
            { 
                featureType: "transit.line", 
                elementType: "geometry", 
                stylers: [{color: "#e5e5e5"}] 
            }, 
            { 
                featureType: "transit.station", 
                elementType: "geometry", 
                stylers: [{color: "#eeeeee"}] 
            }, 
            { 
                featureType: "water", 
                elementType: "geometry", 
                stylers: [{color: "#c9c9c9"}]
            }, 
            { 
                featureType: "water", 
                elementType: "labels.text.fill", 
                stylers: [{color: "#9e9e9e"}]
            } 
        ]
    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(41.646341,41.643392),
        map: map,
        icon: 'img/hg_pin.svg',
        url: 'https://7heaven.ge/'
    });

    var marker2 = new google.maps.Marker({
        position: new google.maps.LatLng(41.641184,41.623589),
        map: map,
        icon: 'img/hg_pin.svg',
        url: 'https://7heaven.ge/'
    });

    var marker3 = new google.maps.Marker({
        position: new google.maps.LatLng(41.640005,41.617495),
        map: map,
        icon: 'img/hg_pin.svg',
        url: 'https://7heaven.ge/'
    });

    var marker4 = new google.maps.Marker({
        position: new google.maps.LatLng(41.643589,41.625509),
        map: map,
        icon: 'img/hg_pin.svg',
        url: 'https://7heaven.ge/'
    });

    google.maps.event.addListener(marker, 'click', function() {
        window.location.href = this.url;
    });

    google.maps.event.addListener(marker2, 'click', function() {
        window.location.href = this.url;
    });

    google.maps.event.addListener(marker3, 'click', function() {
        window.location.href = this.url;
    });

    google.maps.event.addListener(marker4, 'click', function() {
        window.location.href = this.url;
    });
});

var animGear = new Vivus('gear-icon', {type: 'oneByOne', duration: 100, file: '../img/gear2.svg'});
var animChess = new Vivus('chess-icon', {type: 'oneByOne', duration: 100, file: '../img/chess2.svg'});
var animBgLogo = new Vivus('bg-logo-svg', {type: 'oneByOne', duration: 200});